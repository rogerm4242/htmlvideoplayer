# Based on code tutorial here: https://blog.pythonanywhere.com/121/

from flask import Flask, redirect, render_template, request, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from os import listdir as os_listdir

app = Flask(__name__)
app.config["DEBUG"] = True
app.debug = True

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///comments.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

migrate = Migrate(app, db)

class Comment(db.Model):
	__tablename__ = "comments"
	
	id = db.Column(db.Integer, primary_key=True)
	content = db.Column(db.String(4096))

class Playlist(db.Model):
	__tablename__ = "playlist"
	
	id = db.Column(db.Integer, primary_key=True)
	filepath = db.Column(db.String(4096))
	playposition = db.Column(db.Integer)
	
class CurrentVideo(db.Model):
	__tablename__ = "currentvideo"
	
	id = db.Column(db.Integer, primary_key=True)
	currentvideo = db.Column(db.String(4096))
	
@app.route("/", methods=["GET", "POST"])
def index():
	if request.method == "GET":
		# read files in folder
		files = [f for f in os_listdir('static') if f.endswith('.mp4')]
		
		# create list of all videos in database
		videos = [v.filepath for v in Playlist.query.all()]
		
		# create data rows for any new videos
		for f in files:
			if f not in videos:
				entry = Playlist(filepath=f, playposition=0)
				db.session.add(entry)
		db.session.commit()

		# delete data rows if video file no longer is present
		for v in videos:
			if v not in files:
				db.session.query(Playlist).filter(Playlist.filepath==v).delete()
		db.session.commit()
		
		# TODO - show html page with play list of videos
		return render_template("main_page.html", playlist_entries=db.session.query(Playlist).order_by(Playlist.filepath).all())
	
	# comment = Comment(content=request.form["contents"])
	# db.session.add(comment)
	# db.session.commit()
	return redirect(url_for('index'))

@app.route("/update")
def update():
	video = request.args.get('video')
	playposition = request.args.get('playposition')
	print(video, playposition)
	q = db.session.query(Playlist).filter(Playlist.filepath==video)
	record = q.one()
	record.playposition = playposition
	db.session.commit()
	return "ok"
